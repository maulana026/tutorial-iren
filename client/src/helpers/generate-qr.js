import html2canvas from 'html2canvas'

// handler buat download qr code as png file
export default function downloadQr(e) {
    e.preventDefault()
    // get element html berisi qr code
    let img = document.getElementById("qr-image")
    // func buat generate canvas dari html element
    html2canvas(img, {
        backgroundColor: "#ffffff",
        windowWidth: 200,
        windowHeight: 400,
        scale: 2
    }).then(res => {
        // buat html link in javascript way
        let link = document.createElement('a')
        // tentukan nama file 
        link.download = "qr.png"
        // masukin data gambar dari hasil generate oleh html2canvas
        link.href = res.toDataURL()
        // append html link yg dibuat ke 'body'
        document.body.appendChild(link)
        // klik link in javascript way sehingga proses download dimulai
        link.click()
        // remove html link dari body
        document.body.removeChild(link)
    })
}