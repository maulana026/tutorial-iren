async function login(credential, cartId) {

    let options = {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Basic ${credential}`
        },
        body: JSON.stringify({
            cart_id: cartId
        }),
        credentials: 'include'
        };

    let api = await fetch(`${process.env.REACT_APP_API_ENDPOINT}auth/login`, options)
        .then(res => res.json())
        .then(doc => [null,doc])
        .catch(err => [err.toString(),null])

    return api
}

async function profil() {
    
    let options = {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            'Cookie': document.cookie,
        },
        credentials: 'include'
    };
      
    let api = await fetch(`${process.env.REACT_APP_API_ENDPOINT}auth`, options)
        .then(response => response.json())
        .then(res => [ null, res ])
        .catch(error => [ error.toString(), null ])

    return api
}

async function logout() {

    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Cookie': document.cookie
        },
        credentials: 'include'
    }

    let api = await fetch(`${process.env.REACT_APP_API_ENDPOINT}auth/logout`, options)
        .then(response => response.json())
        .then(res => [ null, res ])
        .catch(err => [ err.toString(), null ])

    return api
}

async function register(body) {

    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Cookie': document.cookie
        },
        body: JSON.stringify(body),
        credentials: 'include'
    }

    let api = await fetch(`${process.env.REACT_APP_API_ENDPOINT}auth`, options)
        .then(res => res.json())
        .then(doc => [ null, doc ])
        .catch(err => [ err.toString(), null ])

    return api
}

async function daftarBarangUser() {
    
    let options = {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            'Cookie': document.cookie,
        },
        credentials: 'include'
    };
      
    let api = await fetch(`${process.env.REACT_APP_API_ENDPOINT}product/list`, options)
        .then(response => response.json())
        .then(res => [ null, res ])
        .catch(error => [ error.toString(), null ])

    return api
}

module.exports = { login, profil, logout, register, daftarBarangUser }