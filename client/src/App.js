import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import Scanner from './pages/Scanner'
import ProductOutput from './pages/ProductOutput'
import Warehouse from './pages/Warehouse';
import AmbilBarang from './pages/AmbilBarang';
import SimpanBarang from './pages/SimpanBarang';
import NotFound from './pages/NotFound'
import Gudang from './pages/Gudang'
import PaketInCart from './pages/PaketInCart'
import StatusBarang from './pages/StatusBarang'
import Login from './pages/Login';
import Authentication from './wrappers/authentication';
import Profil from './pages/Profil';
import Register from './pages/Register';
import { createContext, useEffect, useState } from 'react';
import api from "./helpers/api"
import ListBarangUser from './pages/ListBarangUser';
import ProsesPaket from './pages/ProsesPaket';
import StorePaket from './pages/StorePaket';
import PickupPaket from './pages/PickupPaket';
import TakePaket from './pages/TakePaket';

export const AuthContext = createContext()

function App() {

  const [role,setRole] = useState('')

  const value = {
    role, setRole
  }

  useEffect(() => {
    api.profil()
      .then(([err, res]) => {
        if (res && res.status === 200) {
          if (res.body.isAdmin) setRole('admin')
          if (!res.body.isAdmin) setRole('biasa')
        }
        if (res.code === 401) setRole('visitor')
      })
      .catch(err => {
        console.error(err.message)
      })
    
  }, [role])

  return (
    <div className="App">
      <AuthContext.Provider value={value}>
        <Router>
          <Routes>
            <Route path='/' element={<Authentication><Home /></Authentication>} />
            <Route path='/ambil-barang' element={<Authentication><AmbilBarang/></Authentication>} />
            <Route path='/simpan-barang' element={<Authentication><SimpanBarang /></Authentication>} />
            <Route path='/status-barang' element={<Authentication><StatusBarang /></Authentication>} />
            <Route path='/daftar-barang' element={<Authentication><ListBarangUser /></Authentication>} />
            <Route path='/gudang' element={<Authentication><Gudang /></Authentication>} />
            <Route path='/gudang/scan' element={<Authentication><Scanner /></Authentication>} />
            <Route path="/gudang/list" element={<Authentication><PaketInCart /></Authentication>} />
            <Route path='/gudang/peta' element={<Authentication><Warehouse /></Authentication>} />

            <Route path='/gudang/proses' element={<Authentication><ProsesPaket /></Authentication>} />
            <Route path='/gudang/store' element={<Authentication><StorePaket /></Authentication>} />
            <Route path='/pickup' element={<Authentication><PickupPaket /></Authentication>} />
            <Route path="/gudang/take" element={<Authentication><TakePaket /></Authentication>} />

            <Route path='/product-output' element={<Authentication><Gudang /></Authentication>} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/profil' element={<Authentication><Profil /></Authentication>} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </Router>
      </AuthContext.Provider>
    </div>
  );
}

export default App;
