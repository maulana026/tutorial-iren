import { useContext, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom'
import { AuthContext } from '../App';

function Home() {

    const role = useContext(AuthContext)

    const navigate = useNavigate()

    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'admin') navigate('/gudang')
            if (role.role === 'visitor') navigate('/login')
        }
    }, [role])

    return <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware
                    </h2>
                </div>
                <div>
                    <Link to="/status-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Cek Status</Link>
                    <Link to="/daftar-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Daftar Barang</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Home</h1>
                <div className='grid grid-cols-3 gap-4'>
                    <div>
                        <Link to="/simpan-barang" className='block m-2 py-2 px-4 rounded bg-gray-300 text-gray-700'>Simpan Barang</Link>
                        <Link to="/pickup" className='block m-2 py-2 px-4 rounded bg-gray-300 text-gray-700'>Request Pengambilan Barang</Link>
                        <Link to="/ambil-barang" className='block m-2 py-2 px-4 rounded bg-gray-300 text-gray-700'>Ambil Barang</Link>
                    </div>
                    <div></div>
                    <div></div>                    
                </div>
            </div>
        </>
}

export default Home;