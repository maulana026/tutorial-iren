import { useContext, useEffect, useState } from "react"
import { useNavigate, Link } from "react-router-dom"
import QrReader from 'react-qr-scanner'
import { AuthContext } from "../App"

export default function PickupPaket() {

    const role = useContext(AuthContext)
    const nav = useNavigate()
    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'admin') nav('/gudang')
            if (role.role === 'visitor') nav('/login')
        }
    }, [role,nav])

    const [paket, setPaket] = useState()
    const ScanUlang = () => {
        window.location.reload();
    }
    const scanHandler = data => {
        if (data) {
            let requestOptions = {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json",
                    'Cookie': document.cookie,
                },
                credentials: 'include'
            }

            fetch(
            `${process.env.REACT_APP_API_ENDPOINT}product/${data.text}`,
            requestOptions)
            .then(res => res.json())
            .then(data => {
                if (data.code === 401) setPaket(
                    <p>unauthorized</p>
                )
                else if (data.status !== 200) setPaket(
                    <p>{data.body.message}</p>
                )
                else {
                    let barangs = data.body.isi.split(",")
                    setPaket(
                        <div >
                            <p><span className='font-semibold mr-2'>id: </span>{data.body.id}</p>
                            <div><span className='font-semibold mr-2'>status: </span>
                                <span>{data.body.inQueue && !data.body.isProcessed  && `[in queue] - ${new Date(data.body.inQueueOn).toLocaleString()}`}
                                {data.body.isProcessed && !data.body.isStored  && `[Pass] - ${new Date(data.body.isProcessedOn).toLocaleString()}`}
                                {data.body.isStored && !data.body.pickupRequested  && `[Pass] - ${new Date(data.body.isProcessedOn).toLocaleString()}`}
                                {data.body.pickupRequested && !data.body.isTaken  && `[Pass] - ${new Date(data.body.pickupRequestedOn).toLocaleString()}`}
                                {data.body.isTaken && !data.body.inHand  && `[Pass] - ${new Date(data.body.isTakenOn).toLocaleString()}`}
                                {data.body.inHand  && `[Pass] - ${new Date(data.body.inHandOn).toLocaleString()}`}
                                </span>
                            </div>
                            <div >
                                <p className='font-semibold mr-2'>isi:</p>
                                <div>
                                    <table className='table-auto border-collapse border border-slate-500'>
                                        <thead>
                                            <tr>
                                                <th className='border border-slate-600 p-2 font-normal bg-gray-300'>type</th>
                                                <th className='border border-slate-600 p-2 font-normal bg-gray-300'>detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            // extract type dan detail dengan pemisah :
                                            barangs.map((v,k) => {
                                                let [ type, detail ] = v.split(":")
                                                // tampilin
                                                return(
                                                    <tr key={k}>
                                                        <td className='border border-slate-600 p-2'>{type}</td>
                                                        <td className='border border-slate-600 p-2'>{detail}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <button onClick={(e) => Proses(e,data.body.id)} className="bg-red-200 px-5 py-2 w-full mt-4 rounded font-semibold">Req Pickup</button>
                            <button onClick={ScanUlang} className="bg-gray-200 px-5 py-2 w-full mt-4 rounded font-semibold">Scan Ulang</button>
                        </div>
                    )}
                })
            .catch(err => console.log(JSON.stringify(err)))
        }
    }
    const [status,setStatus] = useState()
    function Proses(e,id) {
        e.preventDefault()

        let requestOptions = {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                'Cookie': document.cookie,
            },
            credentials: 'include'
        };
        // kirim data ke api
        fetch( `${process.env.REACT_APP_API_ENDPOINT}product/${id}/pickup` , requestOptions)
            .then(response => response.json())
            .then(result => {
                // jika server return error tampilkan error message nya
                if (result.status !== 200) setStatus(<p>{result.body.message}</p>)
                // jika tidak tampilkan status barang serta qr codenya
                if (result.status === 200) {
                    setStatus(<p className='text-red-400 font-semibold text-center w-full p-3'>{result.body.status}</p>)
                }
            })
            // jikalau error tampilkan di console.log
            .catch(error => {
                console.log('error' + " - " + error)
            });
    }

    return(
    <>
        <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware
                    </h2>
                </div>
                <div>
                    <Link to="/" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Home</Link>
                    <Link to="/status-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Cek Status</Link>
                    <Link to="/daftar-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Daftar Barang</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Req Pengambilan Barang</h1>
                <div className='grid grid-cols-3 gap-4'>
                    <div>
                        <QrReader
                            delay="false"
                            onError={err => console.log(err)}
                            onScan={scanHandler} />
                    </div>     
                    <div>
                        {paket}
                    </div>  
                    <div>
                        {status}    
                    </div>           
                </div>
            </div>  
    </>
    )
}