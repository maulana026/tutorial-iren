import { Link, useLocation, useNavigate } from 'react-router-dom'
import { useContext, useEffect, useMemo, useState } from 'react';
import QrReader from 'react-qr-scanner'
import { AuthContext } from '../App';

const StatusBarang = () => {

    const { search } = useLocation()
    let params = useMemo(() => new URLSearchParams(search), [search])

    const role = useContext(AuthContext)
    const nav = useNavigate()
    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'admin') nav('/gudang')
            if (role.role === 'visitor') nav('/login')
        }
    }, [role, nav])

    const [ idPaket, setIdPaket ] = useState()
    const [ detailPaket, setDetailPaket ] = useState(<></>)

    const scanHandler = data => {
        if (data) {
            setIdPaket(data.text)
        }
    }

    useEffect(() => setIdPaket(params.get("id")), [])

    useEffect(() => {
        let requestOptions = {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                'Cookie': document.cookie,
            },
            credentials: 'include'
        };
        if (idPaket) fetch(
            `${process.env.REACT_APP_API_ENDPOINT}product/${idPaket}`,
            requestOptions
            )
            .then(res => res.json())
            .then(data => {
                if (data.status !== 200) setDetailPaket(
                    <p className='text-red-400 text-center w-full font-semibold'>{data.body.message}</p>
                )
                else {
                    // extract array dari string
                    let barangs = data.body.isi.split(",")
                    setDetailPaket(
                    <div >
                        <p><span className='font-semibold mr-2'>id: </span>{data.body.id}</p>
                        <div><span className='font-semibold mr-2'>status: </span>
                        <ul>
                            <li>in queue <span className='color-green-500 font-bold'>{data.body.inQueue  && `[Pass] - ${new Date(data.body.inQueueOn).toLocaleString()}`}</span></li>
                            <li>processed <span className='color-green-500 font-bold'>{data.body.isProcessed  && `[Pass] - ${new Date(data.body.isProcessedOn).toLocaleString()}`}</span></li>
                            <li>stored <span className='color-green-500 font-bold'>{data.body.isStored  && `[Pass] - ${new Date(data.body.isProcessedOn).toLocaleString()}`}</span></li>
                            <li>pickupRequested <span className='color-green-500 font-bold'>{data.body.pickupRequested  && `[Pass] - ${new Date(data.body.pickupRequestedOn).toLocaleString()}`}</span></li>    
                            <li>taken <span className='color-green-500 font-bold'>{data.body.isTaken  && `[Pass] - ${new Date(data.body.isTakenOn).toLocaleString()}`}</span></li>   
                            <li>in hand <span className='color-green-500 font-bold'>{data.body.inHand  && `[Pass] - ${new Date(data.body.inHandOn).toLocaleString()}`}</span></li>
                        </ul>
                        </div>
                        <p><span className='font-semibold mr-2'>rak: </span>{data.body.rak}</p>
                        <div className='flex'>
                            <p className='font-semibold mr-2'>isi:</p>
                            <div>
                                <table className='table-auto border-collapse border border-slate-500'>
                                    <thead>
                                        <tr>
                                            <th className='border border-slate-600 p-2 font-normal bg-gray-300'>type</th>
                                            <th className='border border-slate-600 p-2 font-normal bg-gray-300'>detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        // extract type dan detail dengan pemisah :
                                        barangs.map((v,k) => {
                                            let [ type, detail ] = v.split(":")
                                            // tampilin
                                            return(
                                                <tr key={k}>
                                                    <td className='border border-slate-600 p-2'>{type}</td>
                                                    <td className='border border-slate-600 p-2'>{detail}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button onClick={() => window.location.reload()} className="bg-gray-200 px-5 py-2 w-full mt-4 rounded font-semibold">Scan Ulang</button>
                    </div>)}
                })
    }, [idPaket])

    return (
        <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware
                    </h2>
                </div>
                <div>
                    <Link to="/" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Home</Link>
                    <Link to="/daftar-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Daftar Barang</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Cek Status Barang</h1>
                <div className='grid grid-cols-2 gap-4'>
                    <div className='max-w-lg'>
                        {/* for qr-reader */}
                        <QrReader
                            delay="false"
                            onError={err => console.log(err)}
                            onScan={scanHandler} />
                    </div>
                    <div className='max-w-lg'>
                        {/* for details */}
                        {detailPaket}
                    </div>         
                </div>
            </div>
        </>
    )
}

export default StatusBarang