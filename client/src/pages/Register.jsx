import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import api from '../helpers/api'

// funtion buat page register
export default function Register() {
    // state buat ngatur teks yg tampil di button buat submit data register
    const [ registerStatus, setRegisterStatus ] = useState('loading...')
    // handler buat nampilin keterangan loading 
    const [ loading, setLoading ] = useState(true)
    // kalau ada error, message errornya diprint ke html lewat state ini
    const [ errors, setErrors ] = useState(<></>)
    // using react-router function to navigate arround frontend page
    const navigate = useNavigate()
    // handler buat ngecek udah pernah register apa belum
    useEffect(() => {
        (
            async () => {
                // get data dari api profil, 
                // kalau get data sukses data masuk di variabel data
                // kalau gagal masuk di variabel err
                let [err, data] = await api.profil()
                // error handling
                if (err) console.log('profile response ', err)
                // jika get data berhasil dan user ada (http statusnya 200)
                // maka redirect ke laman / gunain fungsi react-router tadi
                if (data && data.status === 200) navigate('/')
                // set status loading ke false buat ngilangin keterangan loading
                setLoading(false)
                // atur kata kata di tombol register jadi "Register"
                setRegisterStatus("Register")
            }
        )()
    }, [])

    // handler buat register form
    function formRegisterHandler(e) {
        // nyegah tab ngereload waktu ngesubmit
        e.preventDefault()
        // atur kata di tombol register jadi Registering...
        setRegisterStatus("Registering...")
        // get data name, username (email) & password dari input name, username & password
        // data keduanya kesimpen di variabel e
        let name = e.target['name'].value
        let username = e.target['username'].value
        let password = e.target['password'].value
        // generate credential/bentukan variabel yg diperluin buat authentication di server
        // disini name & password digabungin dengan separatornya :
        let credential = {
            email: username,
            name: name, 
            password: password
        }
        // fetch api register
        api.register(credential).then(([err, res]) => {
            
            // error handler function
            if (err) setErrors(<div>{err}</div>)
            // error handler api
            if (res && res.status !== 200) setErrors(<div className='text-center bg-red-300 p-2 mx-1 my-3 rounded border border-red-500'>{res.body.message}</div>)
            // success handler
            // jika register berhasil redirect ke frontend '/'
            if (res && res.status === 200) {
                setRegisterStatus("Success")
                navigate('/login')
            }

        })
    }

    // bagian htmlnya
    if (!loading) return(
        <div className="flex min-h-screen justify-center items-center bg-gray-300">
            <div className="p-3 rounded shadow bg-white">
                <div className="py-2 mb-3 text-center">
                    <h1>Tutorial</h1>
                    <h3 className="font-semibold">Register</h3>
                </div>
                <div>
                    {errors}
                    <form onSubmit={formRegisterHandler}>
                        <p>Username</p>
                        <input className="p-2 mb-2 rounded w-full bg-gray-200" type="email" name="username" required/>
                        <p>Name</p>
                        <input className="p-2 mb-2 rounded w-full bg-gray-200" type="name" name="name" required/>
                        <p>Password</p>
                        <input className="p-2 mb-2 rounded w-full bg-gray-200" type="password" name="password" required/>
                        <button className="w-full bg-blue-300 rounded p-3 my-3 transition hover:bg-blue-400">{registerStatus}</button>
                        <p className='text-sm italic py-2 text-center'>or</p>
                        <Link to="/login" className="w-full bg-green-300 rounded p-3 my-3 block text-center transition hover:bg-green-400">Login</Link>
                    </form>
                </div>
            </div>
        </div>
    )
    // jika lagi loading maka return tampilan ini
    return(<div className='h-screen w-screen flex justify-center items-center italic'>loading...</div>)
}