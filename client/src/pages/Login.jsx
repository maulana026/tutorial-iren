import { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { AuthContext } from '../App'
import api from '../helpers/api'

// funtion buat page login
export default function Login() {

    const role = useContext(AuthContext)
    const [ adminMode, setAdminMode] = useState(false)

    // handler buat nampilin keterangan loading 
    const [ loading, setLoading ] = useState(true)
    // kalau ada error, message errornya diprint ke html lewat state ini
    const [ errors, setErrors ] = useState(<></>)
    // using react-router function to navigate arround frontend page
    const navigate = useNavigate()
    // handler buat ngecek udah pernah login apa belum
    useEffect(() => {
        console.log(role)
        if (role.role === 'admin') navigate('/gudang')
        if (role.role === 'biasa') navigate('/')
        setLoading(false)
    }, [role, navigate])

    // handler buat login form
    function formLoginHandler(e) {
        // nyegah tab ngereload waktu ngesubmit
        e.preventDefault()
        // get data name & password dari input name & password
        // data keduanya kesimpen di variabel e
        let name = e.target['name'].value
        let password = e.target['password'].value
        let cartId = e.target['cart_id'].value
        // generate credential/bentukan variabel yg diperluin buat authentication di server
        // disini name & password digabungin dengan separatornya :
        let credential = btoa(`${name}:${password}`)
        // fetch api login
        api.login(credential, cartId).then(([err, res]) => {
            
            // error handler function
            if (err) setErrors(<div>{err}</div>)
            // error handler api
            if (res && res.status !== 200) setErrors(<div className='text-center bg-red-300 p-2 mx-1 my-3 rounded border border-red-500'>{res.body.message}</div>)
            // success handler
            // jika login berhasil redirect ke frontend '/'
            if (res && res.status === 200) {

                if (res.body.isAdmin) {
                    role.setRole('admin')
                    navigate('/gudang')
                }
                else {
                    role.setRole('biasa')
                    navigate('/')
                }
            }

        })
    }

    // bagian htmlnya
    if (!loading) return(
        <div className="flex min-h-screen justify-center items-center bg-gray-300">
            <div className="p-3 rounded shadow bg-white">
                <div className="py-2 mb-3 text-center">
                    <h1>Tutorial</h1>
                    <h3 className="font-semibold">Login</h3>
                </div>
                <div>
                    <div className="grid grid-cols-2 gap-4 mt-1 mb-3 text-sm">
                        <button 
                        onClick={() => setAdminMode(false)}
                            className={`px-3 py-2 rounded-full ${!adminMode ? 'bg-gray-300' : undefined} hover:bg-blue-300`}
                            >Pelanggan</button>
                        <button
                            onClick={() => setAdminMode(true)} 
                            className={`px-3 py-2 rounded-full  ${adminMode ? 'bg-gray-300' : undefined} hover:bg-blue-300`}>Admin</button>
                    </div>
                    {errors}
                    <form onSubmit={formLoginHandler}>
                        <p>Username</p>
                        <input className="p-2 mb-2 rounded w-full bg-gray-200" type="email" name="name" required/>
                        <p>Password</p>
                        <input className="p-2 mb-2 rounded w-full bg-gray-200" type="password" name="password" required/>
                        <div className={!adminMode ? 'hidden' : undefined}>
                        <p>Cart ID</p>
                        <input className="p-2 mb-2 rounded w-full bg-gray-200" type="text" name="cart_id"/>
                        </div>
                        <button className="w-full bg-blue-300 rounded p-3 my-3 transition hover:bg-blue-400">Login</button>
                        <p className='text-sm italic py-2 text-center'>or</p>
                        <Link 
                            to="/register" 
                            className="w-full bg-green-300 rounded p-3 my-3 block text-center transition hover:bg-green-400">Register</Link>
                    </form>
                </div>
            </div>
        </div>
    )
    // jika lagi loading maka return tampilan ini
    return(<div className='h-screen w-screen flex justify-center items-center italic'>loading...</div>)
}