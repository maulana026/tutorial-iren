import { useContext, useEffect, useState } from "react";
import QrReader from 'react-qr-scanner'
import { Link, useNavigate } from 'react-router-dom'
import { AuthContext } from "../App";

function Scanner() {

    const role = useContext(AuthContext)
    const nav = useNavigate()
    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'biasa') nav('/')
            if (role.role === 'visitor') nav('/login')
        }
    }, [role, nav])

    const [ idPaket, setIdPaket ] = useState("")
    const [ detailPaket, setDetailPaket ] = useState(<></>)
    const [ info, setInfo ] = useState({
        html: <></>,
        statusAmbil: false,
        statusSimpan: false
    })

    const scanHandler = data => {
        if (data) {
            setIdPaket(data.text)
        }
    }

    const telahDiambil = e => {
        e.preventDefault()

        var raw = JSON.stringify({
        "id_paket": idPaket,
        "id_cart": "cart-1",
        "action": "ambil-gudang"
        });

        let requestOptions = {
        method: 'PUT',
        headers: {"Content-Type": "application/json",'Cookie': document.cookie},
        body: raw,
        credentials: 'include'
        };

        fetch(`${process.env.REACT_APP_API_ENDPOINT}product`, requestOptions)
        .then(response => response.json())
        .then(result => {
            setInfo({
                html: <p className="text-center font-semibold">{result.body.message}</p>,
                statusSimpan: false,
                statusAmbil: true
            })
        })
        .catch(error => console.log('error', error));

    }

    const telahDisimpan = e => {
        e.preventDefault()

        let raw = JSON.stringify({
        "id_paket": idPaket,
        "id_cart": "cart-1",
        "action": "simpan"
        });

        let requestOptions = {
        method: 'PUT',
        headers: {"Content-Type": "application/json",'Cookie': document.cookie},
        body: raw,
        credentials: 'include'
        };

        fetch(`${process.env.REACT_APP_API_ENDPOINT}product`, requestOptions)
        .then(response => response.json())
        .then(result => {
            setInfo({
                html: <p className="text-center font-semibold">{result.body.message}</p>,
                statusAmbil: false,
                statusSimpan: true
            })
        })
        .catch(error => console.log('error', error));

    }

    const ScanUlang = () => {
        window.location.reload();
    }

    useEffect(() => {
        let requestOptions = {
        method: 'GET',
        headers: {"Content-Type": "application/json",'Cookie': document.cookie},
        credentials: 'include'
        };
        if (idPaket !== "") fetch(
            `${process.env.REACT_APP_API_ENDPOINT}product/${idPaket}?id_cart=cart-1`,
            requestOptions)
            .then(res => res.json())
            .then(data => {
                if (data.code === 401) setDetailPaket(
                    <p>unauthorized</p>
                )
                else if (data.status !== 200) setDetailPaket(
                    <p>{data.body.message}</p>
                )
                else {
                    setDetailPaket(
                        <div >
                            <p><span className='font-semibold mr-2'>id: </span>{data.body.barang_id}</p>
                            <p><span className='font-semibold mr-2'>status: </span>{data.body.status}</p>
                            <p><span className='font-semibold mr-2'>rak: </span>{data.body.tempat_rak}</p>
                            <p><span className='font-semibold mr-2'>disimpan: </span>{data.body.created}</p>
                            <p><span className='font-semibold mr-2'>isi: </span>{data.body.isi}</p>
                            <button disable={info.statusSimpan.toString()} onClick={telahDisimpan} className="bg-blue-200 px-5 py-2 w-full mt-8 rounded font-semibold">Disimpan</button>
                            <button disable={info.statusAmbil.toString()} onClick={telahDiambil} className="bg-red-200 px-5 py-2 w-full mt-4 rounded font-semibold">Diambil</button>
                            <button onClick={ScanUlang} className="bg-gray-200 px-5 py-2 w-full mt-4 rounded font-semibold">Scan Ulang</button>
                        </div>)}
                })
            .catch(err => console.log(JSON.stringify(err)))
    }, [idPaket])
    
    return <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware <span className='text-blue-500 text-sm font-normal'>[Gudang]</span>
                    </h2>
                </div>
                <div>
                    <Link to="/gudang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Gudang</Link>
                    <Link to="/gudang/peta" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Peta</Link>
                    <Link to="/gudang/list" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>List</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Scanner</h1>
                <div className='grid grid-cols-3 gap-4'>
                    <div>
                        <QrReader
                            delay="false"
                            onError={err => console.log(err)}
                            onScan={scanHandler} />
                    </div>
                    <div>
                        {detailPaket}
                    </div>
                    <div>
                        {info.html}   
                    </div>                    
                </div>
            </div>               
        </>
}

export default Scanner;