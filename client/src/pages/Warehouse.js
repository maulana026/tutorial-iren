// ngga bisa dipake karena gunain versi react jadul yg udh ngga didukung
import { Samy, SvgProxy } from 'react-samy-svg'; 
import { Link, useNavigate } from 'react-router-dom'
// ganti react-samy-svg dg manipulasi manual svgnya, biar mudah si svgnya dijadiin komponen di react
import Map from '../components/Map' 
import { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../App';

function Warehouse() {

    const role = useContext(AuthContext)
    const nav = useNavigate()
    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'biasa') nav('/')
            if (role.role === 'visitor') nav('/login')
        }
    }, [role, nav])

    const [ listPaket, setListPaket ] = useState({
        html: <></>,
        list: ""
    })
    const [ sensor, setSensor ] = useState({
        temp: 0,
        hum: 0
    })

    useEffect(() => {
        let requestOptions = {
            method: 'GET',
            headers: {"Content-Type": "application/json",'Cookie': document.cookie},
            credentials: 'include'
          };
          
        fetch(`${process.env.REACT_APP_API_ENDPOINT}product`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.status === 200) {
                    let cssList = "w-full"
                    let dataList = result.body.processed.map((val, key) => {
                        cssList += ` processed-${val.rak}`
                            return(<tr key={`processed#${key}`} className="border-b border-gray-400">
                                    <td className='p-4'>{val.id}</td>
                                    <td className='p-4'>processed</td>
                                    <td className='p-4'>{val.rak}</td>
                                </tr>)
                    })
                    dataList = [
                        ...dataList,
                        ...result.body.pengambilanGudang.map((val, key) => {
                            cssList += ` pengambilanGudang-${val.rak}`
                                return(<tr key={`pengambilanGudang#${key}`} className="border-b border-gray-400">
                                        <td className='p-4'>{val.id}</td>
                                        <td className='p-4'>pengambilanGudang</td>
                                        <td className='p-4'>{val.rak}</td>
                                    </tr>)
                        })
                    ]
                    setListPaket({
                        html: dataList,
                        list: cssList
                    })
                }
            })
            .catch(error => console.log('error', error));
    }, [])

    useEffect(() => {
        var requestOptions = {
            method: 'GET',
            headers: {"Content-Type": "application/json",'Cookie': document.cookie},
            credentials: 'include'
          };
          
          fetch(`${process.env.REACT_APP_API_ENDPOINT}sensor/`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.status === 200) {
                    if (result.body) setSensor({
                        temp: result.body.data.temperature,
                        hum: result.body.data.humidity
                    })
                }
            })
            .catch(error => console.log('error', error));
    }, [sensor])

    return <>
        <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
            <div>
                <h2 className='text-lg font-semibold'>
                    Aware <span className='text-blue-500 text-sm font-normal'>[Gudang]</span>
                </h2>
            </div>
            <div>
                    <Link to="/gudang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Gudang</Link>
                    <Link to="/gudang/list" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>List</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
            </div>
        </nav>
        <div className='px-10 py-4'>
            <div>
                <h2 className='text-lg font-bold py-3 mb-1'>Kondisi Ruangan</h2>
                <div className='mb-3 flex'>
                    <div className='mr-3 bg-blue-200 px-3 py-1 rounded'> 
                        {`Temperature: ${sensor.temp} °C`}
                    </div>
                    <div className='bg-green-200 px-3 py-1 rounded'>
                        {`Humidity: ${sensor.hum} %`}
                    </div>
                </div>
            </div>
            <h1 className='text-lg font-bold py-3 mb-10'>Paket</h1>
            <div>
                <div className='flex gap-5 justify-between w-full'>
                    <table>
                        <thead>
                            <tr>
                                <th>ID Paket</th>
                                <th>Status</th>
                                <th>ID Rak</th>
                            </tr>
                        </thead>
                        <tbody>
                            {listPaket.html}
                        </tbody>
                    </table>
                    <div className='bg-gray-200 rounded border-4'>
                        <table className='h-full'>
                        <thead>
                            <tr>
                                <th>Warna</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className='bg-blue-200 border-gray-200 border-4'></td>
                                <td className='py-2 px-4'>Rak tempat paket harus disimpan</td>
                            </tr>
                            <tr>
                                <td className='bg-red-200 border-gray-200 border-4'></td>
                                <td className='py-2 px-4'>Rak tempat paket harus diambil</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
                
                <Map main={listPaket.list} />
            </div>
        </div>
    </>
}

export default Warehouse;