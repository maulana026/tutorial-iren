import { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { QRCodeSVG } from 'qrcode.react'
// library buat generate html element ke canvas
// sehingga dari canvas bisa di convert ke dataUrl
// untuk dapat didownload sebagai png file oleh browser
import html2canvas from 'html2canvas' 
import { AuthContext } from '../App'
const PaketInCart = () => {

    const role = useContext(AuthContext)
    const nav = useNavigate()
    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'biasa') nav('/')
            if (role.role === 'visitor') nav('/login')
        }
    }, [role, nav])

    const [ listPaket, setListPaket ] = useState(<></>)
    const [ modalQr, setModalQr ] = useState(<></>)

    // handler buat download qr code as png file
    function downloadQr(e) {
        e.preventDefault()
        // get element html berisi qr code
        let img = document.getElementById("qr-image")
        // func buat generate canvas dari html element
        html2canvas(img, {
            backgroundColor: "#ffffff",
            windowWidth: 200,
            windowHeight: 400,
            scale: 2
        }).then(res => {
            // buat html link in javascript way
            let link = document.createElement('a')
            // tentukan nama file 
            link.download = "qr.png"
            // masukin data gambar dari hasil generate oleh html2canvas
            link.href = res.toDataURL()
            // append html link yg dibuat ke 'body'
            document.body.appendChild(link)
            // klik link in javascript way sehingga proses download dimulai
            link.click()
            // remove html link dari body
            document.body.removeChild(link)
        })
    }

    const generateQr = (e, id) => {
        e.preventDefault()

        setModalQr(<div className='absoulte inset-0 p-2 flex items-center flex-col'>
            <div id="qr-image" className='flex items-center justify-center flex-col pt-4'>
                <QRCodeSVG value={id} />
                <p className='text-center m-4'>{id}</p>
            </div>  
            <button onClick={downloadQr} className="bg-blue-200 py-2 px-4 rounded hover:bg-blue-500">Download QR-Code</button>  
        </div>)

    }

    useEffect( () => {
        var requestOptions = {
            method: 'GET',
            headers: {"Content-Type": "application/json",'Cookie': document.cookie},
            credentials: 'include'
          };
          
        fetch(`${process.env.REACT_APP_API_ENDPOINT}product`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.status === 200) {
                    let dataList = result.body.processed.map((val, key) => <tr key={key} className="border-b border-gray-400">
                            <td className='p-4'>{val.id}</td>
                            <td className='p-4'>processed</td>
                            <td className='p-4'>{val.rak}</td>
                            <td className='p-4'>
                                <button onClick={(e) => generateQr(e, val.id)} className="bg-red-200 px-3 py-2 rounded">Generate QR  ↗️</button>
                                </td>
                        </tr>)

                    dataList = [ ...dataList,...result.body.pengambilanGudang.map((val, key) => <tr key={key} className="border-b border-gray-400">
                        <td className='p-4'>{val.id}</td>
                        <td className='p-4'>pengambilanGudang</td>
                        <td className='p-4'>{val.rak}</td>
                        <td className='p-4'>
                            <button onClick={(e) => generateQr(e, val.id)} className="bg-red-200 px-3 py-2 rounded">Generate QR  ↗️</button>
                            </td>
                    </tr>)]
                    setListPaket(dataList)
                }
            })
            .catch(error => console.log('error', error));

    }, [])

    return(
        <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware <span className='text-blue-500 text-sm font-normal'>[Gudang]</span>
                    </h2>
                </div>
                <div>
                    <Link to="/gudang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Gudang</Link>
                    <Link to="/gudang/peta" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Peta</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Paket</h1>
                <div className='flex'>
                    <div>
                    <table className='table-auto w-full'>
                        <thead>
                            <tr>
                                <th className='p-2'>Isi Paket</th>
                                <th className='p-2'>Status</th>
                                <th className='p-2'>Rak</th>
                                <th className='p-2'>
                                
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {listPaket} 
                        </tbody>
                    </table>   
                    </div> 
                    {modalQr}             
                </div>
            </div>
        </>
    )
}

export default PaketInCart