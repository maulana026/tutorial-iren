import { Link, useNavigate } from 'react-router-dom'
import { useContext, useEffect, useRef, useState } from "react";
import WebCam from 'react-webcam'
import '@tensorflow/tfjs-backend-cpu'
import '@tensorflow/tfjs-backend-webgl'
import * as cocoSsd from '@tensorflow-models/coco-ssd'
import { QRCodeSVG } from 'qrcode.react'
import downloadQr from '../helpers/generate-qr';
import { AuthContext } from '../App';

const SimpanBarang = () => {

    const role = useContext(AuthContext)
    const nav = useNavigate()
    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'admin') nav('/gudang')
            if (role.role === 'visitor') nav('/login')
        }
    }, [role, nav])

    const [ upStatus, setUpStatus ] = useState("")
    const [ idPaket, setIdPaket ] = useState(<></>)
    const [ formEdit, setFormEdit ] = useState(<></>)
 
    const camRef = useRef()
    const camConstratint = {
        width: 1280,
        height: 720,
        facingMode: "user"
    }

    // handler untuk menyimpan data yg diinput di form 
    const simpanBarang = (e) => {
        e.preventDefault()
        // get jumlah html input buat barang
        let jumlahBarang = e.target.childNodes[0].childNodes.length

        let data = ""
        // generate value dalam inputan di form jadi satu data string
        // dengan pemisahnya ','
        for (let i = 0; i < jumlahBarang; i++) {
            // disesuaikan dengan tingkatan di htmlnya
            // karena disini input element dibungkus dalam beberapa div, maka
            // semakin 'banyak' juga 'childNodes' yang dipake
            const elementType = e.target.childNodes[0].childNodes[i].childNodes[1].lastChild.value
            const elementDetail = e.target.childNodes[0].childNodes[i].childNodes[2].lastChild.value
            data += `${elementType}:${elementDetail}`
            if (i < jumlahBarang-1) data += ","
        }

        // stringify data json yg dikirim ke api
        var raw = JSON.stringify({
            "barang": data,
            "cart": "cart-1"
        });
        // set http request options
        let requestOptions = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Cookie': document.cookie,
            },
            body: raw,
            credentials: 'include'
        };
        // kirim data ke api
        fetch( `${process.env.REACT_APP_API_ENDPOINT}product` , requestOptions)
            .then(response => response.json())
            .then(result => {
                // jika server return error tampilkan error message nya
                if (result.status !== 200) setUpStatus(<p>{result.body.message}</p>)
                // jika tidak tampilkan status barang serta qr codenya
                if (result.status === 200) {
                    setUpStatus(<p className='text-red-400 font-semibold text-center w-full p-3'>{result.body.status}</p>)
                    setIdPaket(<div className='absoulte inset-0 p-2 flex items-center flex-col'>
                    <div id="qr-image" className='flex items-center justify-center flex-col pt-4'>
                        <QRCodeSVG value={result.body.id} />
                        <p className='text-center m-4'>{result.body.id}</p>
                    </div>  
                    <button onClick={downloadQr} className="bg-blue-200 py-2 px-4 rounded hover:bg-blue-500">Download QR-Code</button>  
                </div>)
                }
            })
            // jikalau error tampilkan di console.log
            .catch(error => {
                console.log('error' + " - " + error)
            });
    }

    // handler buat tambahin field input di form
    function addInputFiled(e, dataVar) {
        e.preventDefault()

        // get jumlah html input buat barang
        let jumlahBarang = e.nativeEvent.originalTarget.parentElement.childNodes[0].childNodes.length
        // reset dataVar
        dataVar = ""
        // generate value dalam inputan di form jadi satu data string
        // dengan pemisahnya ','
        for (let i = 0; i < jumlahBarang; i++) {
            const elementType = e.nativeEvent.originalTarget.parentElement.childNodes[0].childNodes[i].childNodes[1].lastChild.value
            const elementDetail = e.nativeEvent.originalTarget.parentElement.childNodes[0].childNodes[i].childNodes[2].lastChild.value
            dataVar += `${elementType}:${elementDetail}`
            if (i < jumlahBarang-1) dataVar += ","
        }

        // jika pengguna ngeklik Tambah Barang berkali kali
        // simpan value di semua inputan sebelumnya
        if (e.nativeEvent.originalTarget.form['tambahan']) {
            dataVar = `${dataVar},
                ${e.nativeEvent.originalTarget.form['tambahan-type'].value}
                :${e.nativeEvent.originalTarget.form['tambahan-detail'].value}`
            console.log(dataVar)
        }
        // generate input filed dari data semua filed input sebelumnya
        let data = dataVar.split(',').map((v, i) => {
            let [type, detail] = v.split(":")
        return(
            <div key={i} className="my-1">
                <p className='w-full mb-2 font-semibold mt-4'>Barang {i+1}</p>
                <div className='flex items-center mb-2'>
                    <p className='w-16 font-semibold m-auto'>Type</p>
                    <input key={i} type="text" name={`barang-type-${i}`} defaultValue={type} className="p-2 rounded w-full bg-gray-200"/>
                </div>
                <div className='flex items-center mb-2'>
                    <p className='w-16 font-semibold m-auto'>Detail</p>
                    <input type="text" name={`barang-detail-${i}`} defaultValue={detail} className="p-2 rounded w-full bg-gray-200"/>
                </div>
            </div>
            )
        })
        // set form baru dengan tambahan satu field input baru
        setFormEdit(
            <form onSubmit={simpanBarang}>
                <ul className='list-inside list-decimal'>
                    {data}
                    <div className="my-1">
                        <p className='w-full mb-2 font-semibold mt-4'>Barang {data.length+1}</p>
                        <div className='flex items-center mb-2'>
                            <p className='w-16 font-semibold m-auto'>Type</p>
                            <input type="text" name="tambahan-type" className="p-2 rounded w-full bg-gray-200"/>
                        </div>
                        <div className='flex items-center mb-2'>
                            <p className='w-16 font-semibold m-auto'>Detail</p>
                            <input type="text" name="tambahan-detail" className="p-2 rounded w-full bg-gray-200"/>
                        </div>
                    </div>
                </ul>
                <button className="bg-blue-200 px-5 py-2 rounded border mt-5 mb-2 w-full" onClick={e => addInputFiled(e, dataVar)}>Tambah Barang</button>
                <button className="bg-gray-200 px-5 py-2 rounded border mt-5 mb-2 w-full">Simpan Barang</button>
            </form>
        )
    }

    const SnapshotWebcam = async e => {
        e.preventDefault()

        const imgSrc = await camRef.current.getScreenshot();

        let imgProd = new Image(720, 360)
        imgProd.src = imgSrc

        setFormEdit(<p>loading...</p>)

        const model = await cocoSsd.load()

        const prediction = await model.detect(imgProd)

        let dataVar = ""
        // generate data kumpulan barang yang kedetek dalam bentuk string
        // dengan pemisahnya ','
        // sekaligus generate field input u/ setiap barang 
        // dalam variabel data
        let data = prediction.map((v, i) => {
            if (i < prediction.length -1) {
                dataVar += v.class +":,"
            } else {
                dataVar += v.class + ":"
            }
            return (<div key={i} className="my-1">
                <p className='w-full mb-2 font-semibold mt-4'>Barang {i+1}</p>
                <div className='flex items-center mb-2'>
                    <p className='w-16 font-semibold m-auto'>Type</p>
                    <input key={i} type="text" name={`barang-type-${i}`} defaultValue={v.class} className="p-2 rounded w-full bg-gray-200"/>
                </div>
                <div className='flex items-center mb-2'>
                    <p className='w-16 font-semibold m-auto'>Detail</p>
                    <input type="text" name={`barang-detail-${i}`} className="p-2 rounded w-full bg-gray-200"/>
                </div>
            </div>)
        })
        console.log(dataVar)
        // tampilkan form 
        setFormEdit(
            <form onSubmit={simpanBarang}>
                <ul className='list-inside list-decimal'>
                    {data}
                </ul>
                <button className="bg-blue-200 px-5 py-2 rounded border mt-5 mb-2 w-full" onClick={e => addInputFiled(e, dataVar)}>Tambah Barang</button>
                <button className="bg-gray-200 px-5 py-2 rounded border mt-5 mb-2 w-full">Simpan Barang</button>
            </form>
        )

    }

    return(
        <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware
                    </h2>
                </div>
                <div>
                    <Link to="/" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Home</Link>
                    <Link to="/status-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Cek Status</Link>
                    <Link to="/daftar-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Daftar Barang</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Simpan Barang</h1>
                <div className='grid grid-cols-3 gap-4'>
                    <div className='flex items-center flex-col'>
                        <WebCam 
                        className="react-webcam"
                        audio={false}
                        height={360}
                        width={720}
                        screenshotFormat="image/jpeg"
                        ref={camRef}
                        videoconstraint={camConstratint}
                        />
                        <button onClick={SnapshotWebcam} className="bg-gray-200 rounded px-5 py-2 m-2 w-full font-semibold">Snap!</button>
                    </div>
                    <div>
                        {formEdit}
                    </div>
                    <div className='flex flex-col items-center'>
                        {idPaket}
                        {upStatus}
                    </div>
                </div>
            </div>
        </>
    )
}

export default SimpanBarang