import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { daftarBarangUser } from '../helpers/api'

const ListBarangUser = () => {
    const [daftar, setDaftar] = useState({loading: true})

    useEffect(() => {
        daftarBarangUser()
        .then(res => {
            let html = []
            let array = res[1].body.data
            for (let i = 0; i < array.length; i++) {
                if (i%2 === 0) html.push(
                    <tr className='bg-gray-100 hover:bg-gray-400' key={i}>
                        <td className='px-2 py-3'>{array[i].isi}</td>
                        <td className='px-2 py-3 text-center'>{new Date(array[i].inQueueOn).toLocaleString()}</td>
                        <td className='px-2 py-3 text-center'>
                            <Link className='bg-blue-300 rounded py-1 px-3 hover:bg-blue-200' to={`/status-barang?id=${array[i].id}`}>detail</Link>
                        </td>
                    </tr>
                ) 
                else html.push(
                    <tr className='bg-gray-300 hover:bg-gray-400' key={i}>
                        <td className='px-2 py-3'>{array[i].isi}</td>
                        <td className='px-2 py-3 text-center'>{new Date(array[i].inQueueOn).toLocaleString()}</td>
                        <td className='px-2 py-3 text-center'>
                            <Link className='bg-blue-300 rounded py-1 px-3 hover:bg-blue-200' to={`/status-barang?id=${array[i].id}`}>detail</Link>
                        </td>
                    </tr>
                )
                
            }
            setDaftar(html)
        })
        .catch(err => setDaftar(JSON.stringify(err.message)))
    },[])
    return (
        <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware
                    </h2>
                </div>
                <div>
                    <Link to="/" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Home</Link>
                    <Link to="/status-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Cek Status</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Daftar Barang</h1>
                <div className='w-full'>
                    <table className="table-auto w-full">
                        <thead className='bg-gray-200'>
                            <tr>
                                <th className='p-2'>Barang</th>
                                <th className='p-2'>Waktu Pengiriman</th>
                                <th className='p-2'>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {!daftar.loading && daftar}
                        </tbody>
                    </table>
                </div>
                </div>
        </>
    )
}

export default ListBarangUser