import { useContext, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { AuthContext } from '../App'

const Gudang = () => {

    const role = useContext(AuthContext)

    const navigate = useNavigate()

    useEffect(() => {
        if (role.role !== '') {
            if (role.role === 'biasa') navigate('/')
            if (role.role === 'visitor') navigate('/login')
        }
    }, [role, navigate])

    return(
        <>
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware <span className='text-blue-500 text-sm font-normal'>[Gudang]</span>
                    </h2>
                </div>
                <div>
                    <Link to="/gudang/peta" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Peta</Link>
                    <Link to="/gudang/list" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>List</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            </nav>
            <div className='px-10 py-4'>
                <h1 className='text-lg font-bold py-3 mb-10'>Gudang</h1>
                <div className='grid grid-cols-3 gap-4'>
                    <div>
                        <Link to="/gudang/proses" className='block m-2 py-2 px-4 rounded bg-gray-300 text-gray-700'>Proses Barang</Link>
                        <Link to="/gudang/store" className='block m-2 py-2 px-4 rounded bg-gray-300 text-gray-700'>Simpan Barang</Link>
                        <Link to="/gudang/take" className='block m-2 py-2 px-4 rounded bg-gray-300 text-gray-700'>Ambil Barang dari Rak</Link>
                    </div>
                    <div></div>
                    <div></div>                    
                </div>
            </div>
        </>
    )
}

export default Gudang