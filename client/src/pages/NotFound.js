import { Link } from 'react-router-dom'

const NotFound = () => {
    return(
        <div className="h-screen">
            <nav className='flex justify-between items-center py-4 px-10 bg-gray-200 absolute inset-x-0'>
                <div>
                    <h2 className='text-lg font-semibold'>
                        Aware
                    </h2>
                </div>
                <div>
                    <Link to="/" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Home</Link>
                    <Link to="/simpan-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Simpan Barang</Link>
                    <Link to="/ambil-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Ambil Barang</Link>
                </div>
            </nav>
            <p className="text-center text-xl font-semibold h-full flex items-center justify-center bg-red-300">Not Found!</p>
        </div>
        
    )
}

export default NotFound