import { useEffect, useState } from 'react';
import QrReader from 'react-qr-scanner'

function ProductOutput() {

    const [ idPaket, setIdPaket ] = useState("")
    const [ detailPaket, setDetailPaket ] = useState(<></>)
    const [ infoPaketDiambil, setInfoPaketDiambil ] = useState({
        html: <></>,
        status: false
    })

    const scanHandler = data => {
        if (data) {
            setIdPaket(data.text)
        }
    }

    const telahDiambil = e => {
        e.preventDefault()

        var raw = JSON.stringify({
        "id_paket": idPaket,
        "action": "cart-1"
        });

        var requestOptions = {
        method: 'PUT',
        headers: {"Content-Type": "application/json"},
        body: raw,
        redirect: 'follow'
        };

        fetch(`${process.env.REACT_APP_API_ENDPOINT}/product`, requestOptions)
            .then(response => response.json())
            .then(result => {
                setInfoPaketDiambil({
                    html: <p>{result.body.message}</p>,
                    status: true
                })
                setDetailPaket(<></>)
            })
            .catch(error => console.log('error', error));
    }

    useEffect(() => {
        if (idPaket !== "") fetch(`${process.env.REACT_APP_API_ENDPOINT}/product/${idPaket}?id_cart=cart-1`)
            .then(res => res.json())
            .then(data => {
                if (data.status !== 200) setDetailPaket(
                    <p>{data.body.message}</p>
                )
                else {
                    setDetailPaket(
                    <div >
                        <p><span>id: </span>{data.body.barang_id}</p>
                        <p><span>status: </span>{data.body.status}</p>
                        <p><span>rak: </span>{data.body.tempat_rak}</p>
                        <p><span>disimpan: </span>{data.body.created}</p>
                        <p><span>isi: </span>{data.body.isi}</p>
                        <button onClick={telahDiambil}>Telah diambil</button>
                    </div>)}
                })
            .catch(err => console.log(JSON.stringify(err)))
    }, [idPaket])

    return <>
        <h1>ProductOutput</h1>
        <div style={{display: "flex"}}>
            <div>
                {/* for qr-reader */}
                <QrReader
                    delay="false"
                    onError={err => console.log(err)}
                    onScan={scanHandler} />
            </div>
            <div>
                {/* for details */}
                {infoPaketDiambil.html}
                {detailPaket}
            </div>
        </div>
    </>
}

export default ProductOutput;