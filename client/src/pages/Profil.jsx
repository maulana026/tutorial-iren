import { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { AuthContext } from '../App'

import Api from '../helpers/api'

export default function Profil() {
    const role = useContext(AuthContext)
    // penyimpan data profil
    const [ data, setData ] = useState([])
    // func buat redirect
    const navigate = useNavigate()

    useEffect(() => {
        // get api profile detal
        Api.profil().then(doc => {
            // set data dari api ke state data
            setData(doc)
        })
    }, [])

    function logoutHandler(state) {
        // jika logout redirect ke /login
        Api.logout().then(res => {
            // jika logout berhasil send 'sinyal' bahwa telah logout
            if (res[1].status) {
                role.setRole('visitor')
                navigate('/login')
            }
        })
    }
    return(
    <>
        <nav className='flex justify-between items-center py-4 px-10 bg-gray-200'>
            <div>
                <h2 className='text-lg font-semibold'>
                    Aware
                </h2>
            </div>
            {
            role.role === 'admin'
            ?   <div>
                    <Link to="/gudang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Gudang</Link>
                    <Link to="/gudang/peta" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Peta</Link>
                    <Link to="/gudang/list" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>List</Link>
                </div>
            :   <div>
                    <Link to="/" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Home</Link>
                    <Link to="/status-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Cek Status</Link>
                    <Link to="/daftar-barang" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Daftar Barang</Link>
                    <Link to="/profil" className='py-2 px-4 mr-2 rounded bg-gray-300 text-gray-700'>Profil</Link>
                </div>
            }
            
        </nav>
        <div className='px-10 py-4'>
            <h1 className='text-lg font-bold py-3 mb-3'>User Profil</h1>
            <div className='grid grid-cols-3 gap-4'>
                {
                // jika state data ada data, tampilkan jsx
                data.length > 0
                ? data[0] // jika ada error dari get data di api 
                ? <div className='bg-red-300 border border-red-800 p-3 rounded w-full'>{data[0]}</div>
                // jika berhasil get data profil dari api
                : data[1] && profilView(data[1].body, logoutHandler)
                : <></>
                }                  
            </div>
        </div>
    </>)
}

// func buat nampilin detail profil
function profilView(data, logout) {
    
    // func buat logout
    function logoutHandler(e) {
        e.preventDefault()

        logout(true)
    }

    return(
        <div>
            <div className='mb-2'>
                <p className='text-sm font-semibold'>Name</p>
                <p className='text-lg'>{data.name}</p>
            </div>
            <div className='mb-2'>
                <p className='text-sm font-semibold'>Email</p>
                <p className='text-lg'>{data.email}</p>
            </div>
            <div className='mt-10'>
                <button
                    onClick={logoutHandler} 
                    className='bg-red-800 text-white transition hover:bg-red-600 px-3 py-1 rounded w-32'
                >Logout</button>
            </div>
        </div>
    )
}