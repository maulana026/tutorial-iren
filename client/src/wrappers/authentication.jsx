import { useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { AuthContext } from "../App"

// function buat middleware auth
// digunain buat mastiin si user udah login apa belum
// alurnya, jika ternyata udah login maka tampilin view yg jadi children
// jika belum login/error lainnya maka tampilin messagenya
export default function Authentication(props) {

    const role = useContext(AuthContext)
    const [ authorized, setAuthorized ] = useState(false)
    const nav = useNavigate()

    useEffect(() => {
        if (role !== '') setAuthorized(role !== 'visitor')
        if (role === 'visitor') {
            setTimeout(() => nav('/login'), 5000)
        }
    }, [role,nav])

    // jika ternyata ada error atau user belom login maka tampilin ini
    if (role === 'visitor') return(
        <div className="w-full h-full flex justify-center items-center">
            <div className="m-auto my-5 bg-red-300 border border-red-500 text-center p-3 rounded text-red-800 font-semibold">Not Authorized</div>
        </div>
    )
    // jika user emng kedeteksi udah login maka ini tampilannya
    if (authorized) return(props.children)
    // jika sistem masih coba buat get data profil maka ini tampilannya
    return (<div className="w-full h-full flex justify-center items-center italic">loading...</div>)
}