require('dotenv').config()

const app = require('express')()
const cors = require('cors')
const cooiieParser = require('cookie-parser')

app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true
}))
app.use(require('express').json())
app.use(cooiieParser('tutorial'))

const logMiddleware = require('./middleware/log.middleware')

app.use(logMiddleware)

const v1 = require('./router/v1')

app.use('/v1', v1)

const resultMiddleware = require('./middleware/result.middleware')

app.use(resultMiddleware)

app.listen(process.env.PORT || 3001)