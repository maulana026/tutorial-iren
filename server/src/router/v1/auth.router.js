const Auth = require('express').Router()

const authController = require('../../controller/auth.controller')
const { authorization, authorizationGeneral } = require('../../middleware/auth.middleware')

Auth.post('/', authController.register)
Auth.post('/login', authController.login)
Auth.get('/', authorizationGeneral, authController.getProfile)
Auth.post('/logout', authorizationGeneral, authController.logout)

Auth.post('/admin', authController.registerAdmin)

module.exports = Auth