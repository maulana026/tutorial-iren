const product = require('express').Router()
const { read, list, write, update, daftarBarang, setProcessed, setStored, setReqPengambilan, setPengambilanGudang, setBarangDiambil } = require('../../controller/product.controller')
const { authorization, authorizationAdmin, authorizationGeneral } = require('../../middleware/auth.middleware')

// penggnaan middleware authorization untuk 
// mencegah user selain role: user biasa
// bisa menggunakan fitur-fitur terkait 
product.get('/:idPaket', authorizationGeneral, read)
product.get('/list', authorization, daftarBarang)
product.get('/', authorizationAdmin, list)
product.post('/', authorization, write)
product.put('/', authorizationGeneral, update)
product.put('/:id/processed', authorizationAdmin, setProcessed)
product.put('/:id/stored', authorizationAdmin, setStored)
product.put('/:id/pickup', authorization, setReqPengambilan)
product.put('/:id/take', authorizationAdmin, setPengambilanGudang)
product.put('/:id/diambil', authorization, setBarangDiambil)

module.exports = product