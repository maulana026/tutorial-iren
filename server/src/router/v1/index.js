const v1 = require('express').Router()

const product = require('./product.router')
const sensor = require('./sensor.router')
const auth = require('./auth.router')

v1.use('/product', product)
v1.use('/sensor', sensor)
v1.use('/auth', auth)

module.exports = v1