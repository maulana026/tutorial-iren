const sensor = require('express').Router()
const { getLatestData } = require('../../controller/sensor.controller')
const { authorizationAdmin } = require('../../middleware/auth.middleware')

sensor.get('/', authorizationAdmin, getLatestData)

module.exports = sensor