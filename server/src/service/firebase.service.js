var admin = require("firebase-admin");

var serviceAccount = require("../../key/firebase.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});


module.exports = {
    firestore: () => admin.firestore()
}