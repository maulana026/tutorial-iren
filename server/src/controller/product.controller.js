const { firestore } = require("../service/firebase.service")
const fbAdmin = require('firebase-admin')
const { v4: uuid } = require('uuid')
const {update} = require('./update.product.controller')
const firebaseService = require("../service/firebase.service")

const write = async (req, res, next) => {

    // get list barang
    const body = req.body
    const barang = body.barang

    if (barang == undefined) {
        
        req.status = 400
        req.response = {
            message: "data tidak lengkap"
        }

        next()
        return
    }


    try {

        // find rak kosong (empty == true)
        const rak = await firestore().collection('rak')
                        .where('empty', "==", true)
                        .limit(1)
                        .get()
        
        if (rak.size < 1) throw new Error("gagal mencari rak")

        /* list status paket:
            - in queue (customer)
            - processed (gudang)
            - stored (gudang)
            - pickup request (customer)
            - taken (gudang)
            - in hand (customer)
        */
        const paket = await firestore().collection('paket')
            .add({
                isi: barang,
                rak: rak.docs[0].id,
                inQueue: true,
                isProcessed: false,
                isStored: false,
                pickupRequested: false,
                isTaken: false,
                inHand: false,
                processedBy: null,
                storedBy: null,
                takenBy: null,
                inQueueOn: fbAdmin.firestore.FieldValue.serverTimestamp(),
                pemilik: req.auth.data.email
            })
        
            await firestore().collection('rak').doc(rak.docs[0].id)
                .update({
                    empty: false
                })

        req.status = 200
        req.response = {
            status: 'in queue',
            id: paket.id
        }

        next()
    
    } catch(err) {
        console.log(err)

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
    }
}

const setProcessed = async (req, res, next) => {
    try {
        const id = req.params['id']

        let paket = await firestore().collection('paket')
            .doc(id).get()

        if (paket.get('isProcessed')) throw new Error('409#paket')

        let cart = await firestore().collection('cart')
            .where('lastUser', '==', req.auth.data.email)
            .get()

        let cartData = cart.docs[0].data()

        if (cartData.paketStored+1 >= cartData.maxPaket) throw new Error('409#cartfull')

        cart.docs[0].ref.update({
            paketStored: cartData.paketStored+1
        })

        await paket.ref.update({
                isProcessed: true,
                isProcessedBy: req.auth.data.email,
                isProcessedOn: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

        req.status = 200
        req.response = {
            status: 'is processed'            
        }
    
        next()
    } catch(err) {
        console.error('[setProcessed] ', err.message)

        if (err.message == '409#cartfull') {
            req.status = 409
            req.response = {
                message: 'cart is full'
            }

            return next()
        }
        if (err.message == '409#paket') {
            req.status = 409
            req.response = {
                message: 'paket been processed before'
            }

            return next()
        }

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
    }
}

const setStored = async (req, res, next) => {
    try {
        const id = req.params['id']

        let paket = await firestore().collection('paket')
            .doc(id).get()

        if (paket.get('isStored')) throw new Error('409#paket')

        let cart = await firestore().collection('cart')
            .where('lastUser', '==', req.auth.data.email)
            .get()

        let cartData = cart.docs[0].data()

        cart.docs[0].ref.update({
            paketStored: cartData.paketStored-1
        })

        if (!paket.get('isProcessed')) throw new Error('403#paket')

        await paket.ref.update({
                isStored: true,
                isStoredBy: req.auth.data.email,
                isStoredOn: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

        req.status = 200
        req.response = {
            status: 'is stored'            
        }
    
        next()
    } catch(err) {
        console.error('[setStored] ', err.message)

        if (err.message === "403#paket") {
            req.status = 403
            req.response = {
                message: 'tidak sesuai prosedur'
            }

            return next()
        }
        if (err.message == '409#paket') {
            req.status = 409
            req.response = {
                message: 'paket been stored before'
            }

            return next()
        }

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
    }
}
 
const read = async (req, res, next) => {

    try {

        const { idPaket } = req.params

        const paket = await firestore().collection('paket')
            .doc(idPaket)
            .get()

        if (!paket.exists) throw new Error("paket tidak ada")

        req.status = 200
        req.response = {
            ...paket.data(),
            id: paket.id,
            inQueueOn: paket.data().inQueueOn.toDate(),
            isProcessedOn: paket.data().isProcessedOn && paket.data().isProcessedOn.toDate(),
            pickupRequestedOn: paket.data().pickupRequestedOn && paket.data().pickupRequestedOn.toDate(),
            isTakenOn: paket.data().isTakenOn && paket.data().isTakenOn.toDate(),
            inHandOn: paket.data().inHandOn && paket.data().inHandOn.toDate(),
        }

        next()
    } catch(err) {
        console.log(err)
        req.status = 500
        req.response = {
            message: err.message
        }
        next()
    }

}

const list = async (req,res, next) => {

    let email = req.auth.data.email
    let pakets = {
        processed: []
    }

    try {

        const processed = await firestore()
            .collection('paket')
            .where("isProcessedBy", "==", email)
            .where("isStored", "==", false)
            .get()

        pakets.processed = processed.docs.map((v,i) => {
            return {
                ...v.data(),
                id: v.id,
                pickupRequestedOn: v.data().pickupRequestedOn ? v.data().pickupRequestedOn.toDate() : null,
                isTakenOn: v.data().isTakenOn ? v.data().isTakenOn.toDate() : null,
                isStoredOn: v.data().isStoredOn ? v.data().isStoredOn.toDate() : null,
                inQueueOn: v.data().inQueueOn ? v.data().inQueueOn.toDate() : null,
                inHandOn: v.data().inHandOn ? v.data().inHandOn.toDate() : null,
                isProcessedOn: v.data().isProcessedOn ? v.data().isProcessedOn.toDate() : null}
        })

        const pengambilanGudang = await firestore()
            .collection('paket')
            .where("isTakenBy", "==", email)
            .where("inHand", "==", false)
            .get()

        if (processed.empty && pengambilanGudang.empty) throw new Error("404#paket")

        pakets.pengambilanGudang = pengambilanGudang.docs.map((v,i) => {
            return {
                ...v.data(),
                id: v.id,
                pickupRequestedOn: v.data().pickupRequestedOn ? v.data().pickupRequestedOn.toDate() : null,
                isTakenOn: v.data().isTakenOn ? v.data().isTakenOn.toDate() : null,
                isStoredOn: v.data().isStoredOn ? v.data().isStoredOn.toDate() : null,
                inQueueOn: v.data().inQueueOn ? v.data().inQueueOn.toDate() : null,
                inHandOn: v.data().inHandOn ? v.data().inHandOn.toDate() : null,
                isProcessedOn: v.data().isProcessedOn ? v.data().isProcessedOn.toDate() : null}
        })

        req.status = 200
        req.response = {
            ...pakets
        }

        next()
        return

    } catch(err) {

        if (err.message === '404#paket') {
            req.status = 500
            req.response = {
                message: 'paket not found'
            }

            next()
            return
        }

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
        return
    }
    

}

const daftarBarang = async (req, res, next) => {

    let email = req.auth.data.email

    try {
        let findBarang = await firestore()
            .collection('paket')
            .where("pemilik", "==", email)
            .where("isTaken", "==", false)
            .get()
        
        if (findBarang.empty) throw new Error(404)

        let barang = findBarang.docs.map((v) => {
            return {
                ...v.data(),
                id: v.id,
                pickupRequestedOn: v.data().pickupRequestedOn ? v.data().pickupRequestedOn.toDate() : null,
                isTakenOn: v.data().isTakenOn ? v.data().isTakenOn.toDate() : null,
                isStoredOn: v.data().isStoredOn ? v.data().isStoredOn.toDate() : null,
                inQueueOn: v.data().inQueueOn ? v.data().inQueueOn.toDate() : null,
                inHandOn: v.data().inHandOn ? v.data().inHandOn.toDate() : null,
                isProcessedOn: v.data().isProcessedOn ? v.data().isProcessedOn.toDate() : null
            }
        })

        req.status = 200
        req.response = {
            data: barang
        }

        next()
    } catch (error) {
        console.error('error: ' + error.message)

        if (error.message === '404') {
            req.status = 404
            req.response = {
                message: "Barang not found"
            }

            next()
        }
        req.status = 400
        req.response = {
            message: "Bad Request"
        }

        next()
    }
    

    

}


const setReqPengambilan = async (req, res, next) => {
    try {
        const id = req.params['id']

        const isProcessed = await firestore().collection('paket')
            .doc(id).get()

        // cek apakah paket udah ngelewatin proses sebelumnya apa belom
        // jika belom maka return error 403 
        if (!isProcessed.get('isStored')) throw new Error('403#paket')

        await firestore().collection('paket')
            .doc(id).update({
                pickupRequested: true,
                pickupRequestedBy: req.auth.data.email,
                pickupRequestedOn: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

        req.status = 200
        req.response = {
            status: 'pickup req recorded'            
        }
    
        next()
    } catch(err) {
        console.error('[setReqPengambilan] ', err.message)

        if (err.message === "403#paket") {
            req.status = 403
            req.response = {
                message: 'tidak sesuai prosedur'
            }

            return next()
        }

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
    }
}

const setPengambilanGudang = async (req, res, next) => {
    try {
        const id = req.params['id']

        let paket = await firestore().collection('paket')
            .doc(id).get()

        if (paket.get('isTaken')) throw new Error('409#paket')

        let cart = await firestore().collection('cart')
            .where('lastUser', '==', req.auth.data.email)
            .get()

        let cartData = cart.docs[0].data()

        if (cartData.paketStored+1 >= cartData.maxPaket) throw new Error('409#cartfull')

        cart.docs[0].ref.update({
            paketStored: cartData.paketStored+1
        })

        // cek apakah paket udah ngelewatin proses sebelumnya apa belom
        // jika belom maka return error 403 
        if (!paket.get('pickupRequested')) throw new Error('403#paket')

        await paket.ref.update({
                isTaken: true,
                isTakenBy: req.auth.data.email,
                isTakenOn: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

        req.status = 200
        req.response = {
            status: 'paket taken out from rak'            
        }
    
        next()
    } catch(err) {
        console.error('[setPengambilanGudang] ', err.message)

        if (err.message === "403#paket") {
            req.status = 403
            req.response = {
                message: 'tidak sesuai prosedur'
            }

            return next()
        }
        if (err.message == '409#cartfull') {
            req.status = 409
            req.response = {
                message: 'cart is full'
            }

            return next()
        }
        if (err.message == '409#paket') {
            req.status = 409
            req.response = {
                message: 'paket been taken before'
            }

            return next()
        }

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
    }
}

const setBarangDiambil = async (req, res, next) => {
    try {
        const id = req.params['id']

        const paket = await firestore().collection('paket')
            .doc(id).get()

        if (paket.get('inHand')) throw new Error('409#paket')

        // cek apakah paket udah ngelewatin proses sebelumnya apa belom
        // jika belom maka return error 403 
        if (!paket.get('isTaken')) throw new Error('403#paket')

        const userInDuty = paket.get('isTakenBy')

        const findCartInDuty = await firestore().collection('cart')
            .where("lastUser", "==", userInDuty).get()

        const cartInDuty = findCartInDuty.docs[0]

        await cartInDuty.ref.update({
            paketStored: cartInDuty.get('paketStored')-1
        })

        await firestore().collection('rak').doc(paket.get('rak'))
            .update({
                empty: true
            })

        await paket.ref.update({
                inHand: true,
                inHandBy: req.auth.data.email,
                inHandOn: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

        
        req.status = 200
        req.response = {
            status: 'paket diambil'            
        }
    
        next()
    } catch(err) {
        console.error('[setBarangDiambil] ', err.message)

        if (err.message === "403#paket") {
            req.status = 403
            req.response = {
                message: 'tidak sesuai prosedur'
            }

            return next()
        }
        if (err.message === "409#paket") {
            req.status = 403
            req.response = {
                message: 'paket telah diambil sebelumnya'
            }

            return next()
        }

        req.status = 500
        req.response = {
            message: err.message
        }

        next()
    }
}

module.exports = {

    read: read,
    list: list,
    write: write,
    update: update,
    daftarBarang: daftarBarang,
    setProcessed: setProcessed,
    setStored: setStored,
    setReqPengambilan: setReqPengambilan,
    setPengambilanGudang: setPengambilanGudang,
    setBarangDiambil: setBarangDiambil
}

