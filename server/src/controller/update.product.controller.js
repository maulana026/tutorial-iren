const { firestore } = require("../service/firebase.service")
const fbAdmin = require('firebase-admin')

const update = async (req, res, next) => {

    let dataPaket, errVal
    let isError = false

    // get id paket
    const body = req.body
    const idPaket = body.id_paket
    const idCart = body.id_cart
    const action = body.action
    
    if (idPaket == undefined) {
        req.status = 400
        req.response = {
            message: "tidak ada id_paket"
        }

        next()
        return
    }

    if (idCart == undefined && action == undefined) {
        req.status = 400
        req.response = {
            message: "action no defined"
        }

        next()
        return
    }

    try {

        // get paket detail from firestore
        errVal = "paket tidak ada di cart"
        await firestore().collection('history')
        .where("barang_id", "==", idPaket)
        .get()
        .then(doc => {
            doc.forEach(docs => {
                if (docs.exists) dataPaket = docs.data()
                else isError = true
                console.log(docs.data())
            })
        })

        if (isError) {
            req.status = 400
            req.response = {
                message: errVal
            }

            next()
            return
        }

        // if (idCart !== undefined && action !== "ambil") {

        //     // update rak tempat nyimpan paketnya jadi kondisi kosong
        //     let rakPosition = dataPaket.tempat_rak
        //     await firestore().collection('rak').doc(rakPosition).update({ empty: true })

        //     // delete paket dari cart


        //     // add deletion paket activity to history
        //     await firestore().collection('history')
        //                     .add({
        //                         ...dataPaket,
        //                         status: "diambil",
        //                         proccedAt: fbAdmin.firestore.FieldValue.serverTimestamp()
        //                     })

        //     req.status = 200
        //     req.response = {
        //         message: "pengambilan barang telah terdata"
        //     }

        //     next()
        //     return

        // }

        if (!idCart && action == "ambil") {
            
            await firestore().collection('cart').doc(dataPaket.cart_in_charge)
                .collection('barang').doc(idPaket).update({
                    status: "pengambilan"
                })
            
            await firestore().collection('history').add({
                ...dataPaket,
                status: "pengambilan",
                proccedAt: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

            req.status = 200
            req.response = {
                message: "request pengambilan barang terkirim"
            }

            next()
            return
        }

        if (idCart && action == "simpan") {
            await firestore().collection('cart').doc(idCart)
                .collection('barang').doc(idPaket).update({
                    status: "disimpan"
                })
            
            await firestore().collection('history').add({
                ...dataPaket,
                status: "disimpan",
                proccedAt: fbAdmin.firestore.FieldValue.serverTimestamp()
            })

            req.status = 200
            req.response = {
                message: "penyimpanan barang telah terdata"
            }
    
            next()
            return
        }
        
        if (idCart && action == "ambil-gudang") {
            await firestore().collection('cart').doc(idCart)
                .collection('barang').doc(idPaket).update({
                    status: "diambil-gudang"
                })
            
                await firestore().collection('history').add({
                    ...dataPaket,
                    status: "diambil-gudang",
                    proccedAt: fbAdmin.firestore.FieldValue.serverTimestamp()
                })
                await firestore().collection('cart').doc(idCart)
                    .collection('barang').doc(idPaket)
                    .delete()
                await firestore().collection('rak').doc(dataPaket.tempat_rak)
                    .update({empty: true})
    
                req.status = 200
                req.response = {
                    message: "pengambilan barang telah terdata"
                }
        
                next()
                return
        }


    } catch(err) {
        req.status = 500
        req.response = {
            message: errVal
        }
    }
    
    req.status = 400
    req.response = {
        message: "Bad Request"
    }

    next()
}

module.exports = {
    update
}