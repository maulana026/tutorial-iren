const axios = require('axios');

const getLatestData = async (req, res, next) => {

    let data

    let config = {
        method: 'get',
        url: 'https://platform.antares.id:8443/~/antares-cse/antares-id/w-shipper/dht22/la',
        headers: { 
            'X-M2M-Origin': '1a8f1cea5d320008:e43829b4dd16f180', 
            'Content-Type': 'application/json;ty=4', 
            'Accept': 'application/json'
        }
    };

    /*
        error "fetch is not defined" menandakan fetch sbg library yg digunakan tidak ter'instal' di project
        fetch merupakan library bawaan javascript, fetch tidak tersedia by default dari nodejsnya vercel, karenanya
        kita pake library lain yaitu axios
    */

    await axios(config)
        .then(function (response) {
            data = JSON.parse(response.data["m2m:cin"].con)
        })
        .catch(function (error) {
            console.log(error);
        });

    req.status = 200
    req.response = {
        data
    }

    next()
}

module.exports = {
    getLatestData
}