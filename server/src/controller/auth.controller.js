const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const { firestore } = require('../service/firebase.service')

async function registerAdmin(req,res,next) {

    let { 
        name, email, password
    } = req.body

    let req_error = []
    if (!name) req_error.push('name data is not completed')
    if (!email) req_error.push('email data is not completed')
    if (!password) req_error.push('password data is not completed')

    if (req_error.length > 0) {
        req.status = 400
        req.response = {
            error: req_error
        }
        return next()
    }
    
    // check apakah email sebelumnya udah ada apa belum di firestore
    let isDuplicate = await firestore().collection('userAdmin')
        .doc(email)
        .get().then(res => res.exists).catch(err => {
            console.log('REGIST USER ERROR', err.toString())
        })

    // handler jika email kedetek duplikat
    if (isDuplicate) {
        req.status = 400
        req.response = {
            error:[ 'email has been registered before']
        }
        return next()
    }

    // encrypt pwd
    const salt = bcrypt.genSaltSync(5);
    let encryptedPassword = bcrypt.hashSync(password, salt)

    // write data user baru
    let writeUser = await firestore().collection('userAdmin')
        .doc(email)
        .set({
            name: name,
            email: email,
            password: encryptedPassword
        }).then(res => true)
        .catch(err => {
            console.log('REGIST USER ERROR', err.toString())
            return false
        })

    if (writeUser) {
        req.status = 200
        req.response = {
            success: true
        }
        return next()
    }

    req.status = 500
    req.response = {
        error: ['unidentified server error']
    }
    return next()
} 

async function register(req ,res ,next) {

    // get req body data
    let { 
        name, email, password
    } = req.body

    // handler apabila data di body tidak lengkap (tidak masukin data/masukin string tapi cuman => "" doang)
    let req_error = []
    if (!name) req_error.push('name data is not completed')
    if (!email) req_error.push('email data is not completed')
    if (!password) req_error.push('password data is not completed')

    if (req_error.length > 0) {
        req.status = 400
        req.response = {
            error: req_error
        }
        return next()
    }

    // check apakah email sebelumnya udah ada apa belum di firestore
    let isDuplicate = await firestore().collection('user')
        .doc(email)
        .get().then(res => res.exists).catch(err => {
            console.log('REGIST USER ERROR', err.toString())
        })

    // handler jika email kedetek duplikat
    if (isDuplicate) {
        req.status = 400
        req.response = {
            error:[ 'email has been registered before']
        }
        return next()
    }

    // encrypt pwd
    const salt = bcrypt.genSaltSync(5);
    let encryptedPassword = bcrypt.hashSync(password, salt)

    // write data user baru
    let writeUser = await firestore().collection('user')
        .doc(email)
        .set({
            name: name,
            email: email,
            password: encryptedPassword
        }).then(res => true)
        .catch(err => {
            console.log('REGIST USER ERROR', err.toString())
            return false
        })

    if (writeUser) {
        req.status = 200
        req.response = {
            success: true
        }
        return next()
    }

    req.status = 500
    req.response = {
        error: ['unidentified server error']
    }
    return next()
}

async function login(req, res, next) {

    let isAdmin = false

    // get authorization data from headers
    let { authorization } = req.headers
    const { cart_id } = req.body

    // extract username (email) & pwd
    let [method, str] = authorization.split(' ')
    if (method == 'Basic') authorization = str
    else {
        req.status = 400
        req.response = {
            error: [ 'wrong auth method' ]
        }
        return next()
    }
    let extractStr = new Buffer.from(authorization, 'base64')
    let [ uname, pwd ] = extractStr.toString().split(":")

    try {
        // find uname(email) & pwd in db
        let find = await firestore().collection('user').doc(uname)
            .get().then(async res => {
                if (res.exists) {
                    console.log('aku')
                    return await bcrypt.compare(pwd, res.data().password)}
                else {
                    console.log('dia')
                    return false
                }
            })

        if (find === false) {
            find = await firestore().collection('userAdmin').doc(uname)
                .get().then(async res => {
                    if (res.exists) {
                        return await bcrypt.compare(pwd, res.data().password)}
                    else {
                        return false
                    }
                })

            if (!find) throw(new Error(400))

            isAdmin = true
            if (!cart_id) throw new Error('Field cart_id not completed')
            let findCart = await firestore().collection('cart')
                .doc(cart_id).get()

            console.log(findCart.data().isUsed)
            if (!findCart.data().isUsed) await firestore().collection('cart').doc(cart_id).update({
                isUsed: true,
                lastUser: uname
            })
            else throw new Error('Cart used by another admin')
        }

    // logic if uname & pwd exsist
        if (find === true) {

            // get private key
            // private key can generated by online or get from your domain server
            let privateKey = process.env.PRIVATE_KEY
            
            // generate jwt token with email & current date data
            let token = jwt.sign(
                { isAdmin: isAdmin, email: uname, date: new Date().toISOString() }, 
                privateKey
            )

            // save it to client signedCookie for next procedure
            res.cookie('jwt_token', token, { signed: true })

            // send response to user 
            req.status = 200
            req.response = {
                message: 'success',
                isAdmin
            }

            return next()
        }

    } catch(err) {
        console.log(err.message)
        if (err.message === 'Field cart_id not completed') {
            req.status = 400
            req.response = {
                message: err.message
            }

            return next()
        }
        if (err.message === 'Cart used by another admin') {
            req.status = 400
            req.response = {
                message: err.message
            }

            return next()
        }
        if (err.message === "400") {
            req.status = 400
            req.response = {
                message: 'wrong email or password'
            }

            return next()
        }

        req.status = 500
        req.response = {
            error: [ 'unidentified server error']
        }
        return next()
    }

    
}

async function getProfile(req, res, next) {

    // get data email dari hasil authorisasi
    let email = req.auth.data.email
    let isAdmin = req.auth.data.isAdmin

    // get data user dari firestore berdasarkan email
    // password di 'undefined' untuk mengecualikan password di http response
    let user = email && await firestore().collection(`user${isAdmin ? 'Admin' : ''}`)
        .doc(email).get()
        .then(res => {
            if (res.exists) return {...res.data(), password: undefined, isAdmin}
            else return null
        })
        .catch(err => {
            console.log('GET PROFILE => ', err.toString())
            return null
        })

    // apabila tidak ada data user di firestore maka return http response
    if (user === null) {
        req.status = 400
        req.response = {
            error: 'user not found'
        }

        return next()
    }

    // apabila logic sebelumnya (tidak ada user) tidak berlaku maka 
    // return http response ini
    req.status = 200
    req.response = user
    return next()
}

async function logout(req, res, next) {

    const { email, isAdmin } = req.auth.data

    try {

        if (isAdmin) {
            const findCart = await firestore().collection('cart')
            .where("lastUser", "==", email).get()
            if (!findCart) throw new Error('user not found')

            findCart.docs[0].ref.update({
                isUsed: false
            })

            res.cookie('jwt_token', '', { signed: true })

        } else {
            res.cookie('jwt_token', '', { signed: true })
        }

        req.status = 200
        req.response = {
            status: 'success'
        }
    
        return next()
    } catch(err) {
        console.log(err.message)

        req.status = 500
        req.response = {
            error: err.message
        }

        return next()
    }
    
}
module.exports = { register, login, getProfile, logout, registerAdmin }