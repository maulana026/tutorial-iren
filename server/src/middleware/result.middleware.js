module.exports = (req, res, next) => {

    console.log(req.status)

    let message =   req.status == 500 ? "Internal Server Error" : 
                    req.status == 200 ? "Success" : 
                    req.status == 400 ? "User Side Error" :
                    req.status == 403 ? "Forbidden" :
                    "Not Found"
    
    res.status(req.status)
    res.json({
        status: req.status,
        message: message,
        body: req.response
    })

    next()
}