const jwt = require('jsonwebtoken')

function authorization(req, res, next) {

    // get token jwt yg disimpan di signedCookie
    let token = req.signedCookies['jwt_token']

    // return http response apabila tidak ada token jwt di cookie 
    if (!token) {
        res.status(401)
        return res.json({
            code: 401,
            message: 'unauthorized',
            body: null
        })
    }

    // get private key & verify token jwt di cookie
    let privateKey = process.env.PRIVATE_KEY
    let verifyToken = jwt.verify(token, privateKey)
    console.log(verifyToken)

    // apabila token jwt verified, kirim data ke controller lewat req.auth
    // data berupa status verifikasi jwt & data email yg dibawa di jwt
    // verifyToken.isAdmin mengambil field isAdmin yang telah ditambahkan saat
    // pembentukan token jwt saat login
    if (verifyToken !== undefined && !verifyToken.isAdmin) {
        req.auth = {
            status: true,
            data: verifyToken
        }
        return next()
    }

    // apabila tidak ada case yg sesuai di alur sebelumnya, maka 
    // return http response unauthorized
    res.status(401)
    return res.json({
        code: 401,
        message: 'unauthorized',
        body: null
    })
}

function authorizationAdmin(req, res, next) {

    // get token jwt yg disimpan di signedCookie
    let token = req.signedCookies['jwt_token']

    // return http response apabila tidak ada token jwt di cookie 
    if (!token) {
        res.status(401)
        return res.json({
            code: 401,
            message: 'unauthorized',
            body: null
        })
    }

    // get private key & verify token jwt di cookie
    let privateKey = process.env.PRIVATE_KEY
    let verifyToken = jwt.verify(token, privateKey)

    // apabila token jwt verified, kirim data ke controller lewat req.auth
    // data berupa status verifikasi jwt & data email yg dibawa di jwt
    // verifyToken.isAdmin mengambil field isAdmin yang telah ditambahkan saat
    // pembentukan token jwt saat login
    if (verifyToken !== undefined && verifyToken.isAdmin) {
        req.auth = {
            status: true,
            data: verifyToken
        }
        return next()
    }

    // apabila tidak ada case yg sesuai di alur sebelumnya, maka 
    // return http response unauthorized
    res.status(401)
    return res.json({
        code: 401,
        message: 'unauthorized',
        body: null
    })
}

// auth middleware for both user (admin & biasa)
function authorizationGeneral(req, res, next) {

    // get token jwt yg disimpan di signedCookie
    let token = req.signedCookies['jwt_token']

    // return http response apabila tidak ada token jwt di cookie 
    if (!token) {
        res.status(401)
        return res.json({
            code: 401,
            message: 'unauthorized',
            body: null
        })
    }

    // get private key & verify token jwt di cookie
    let privateKey = process.env.PRIVATE_KEY
    let verifyToken = jwt.verify(token, privateKey)

    // jika token jwt valid, acc
    if (verifyToken !== undefined) {
        req.auth = {
            status: true,
            data: verifyToken
        }
        return next()
    }

    // apabila tidak ada case yg sesuai di alur sebelumnya, maka 
    // return http response unauthorized
    res.status(401)
    return res.json({
        code: 401,
        message: 'unauthorized',
        body: null
    })
}

module.exports = { authorization, authorizationAdmin, authorizationGeneral }